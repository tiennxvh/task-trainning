const express = require('express');
const connectDB = require('./src/config/connectDB.JS');
const bodyParser = require('body-parser');
const initWebRoutes = require('./src/route');
require('dotenv').config();

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

connectDB();
initWebRoutes(app);

const server = app.listen(process.env.PORT, process.env.HOST, () => {
  console.info(`Server running on port http://${process.env.HOST}:${server.address().port}`)
})