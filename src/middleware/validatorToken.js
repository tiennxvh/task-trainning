import fs from 'fs';
import path from 'path';
import db from "../models/index";

const jwt = require('jsonwebtoken');
const publicKey = fs.readFileSync(path.join(__dirname, '../key/public_key.pem'));

const verifyToken =  async (req, res, next) => {
  if (['/login', '/refresh-token'].includes(req.url)) {
    next();
    return;
  }

  const authHeader = req.header('Authorization');
  const token = authHeader && authHeader.split(' ')[1];

  try {
    const decode = jwt.verify(token, publicKey, { algorithms: ['RS256'] });
    req.username = decode.username;
    req.email = decode.email;

    const users = await db.User.findOne({
      where: { email: decode.email }
    });

    req.roleId = users.roleId;

    next();
  } catch (error) {
    // next(error);
    res.status(401).json({ error: 'Invalid token' });
  }
}

module.exports = verifyToken;