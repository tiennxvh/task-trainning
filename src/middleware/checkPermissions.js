import db from "../models/index";

const checkPermissions = async (req, res, next) => {
  const { roleId, path, method, params } = req;
  let nMethod = method.toLowerCase();
  const nPath = path.split('/')[1];
  
  const role = await db.Role.findOne({
    where: { id: roleId }
  })

  const { permissions } = role;

  if (nMethod === 'get' && Object.keys(params).length === 0) {
    nMethod += 'All';
  }

  if (permissions?.includes(`${nPath}.${nMethod}`)) {
    next();
  } else {
    res.status(403).json({ message: "Permission denied" });
  }
}

module.exports = checkPermissions;
