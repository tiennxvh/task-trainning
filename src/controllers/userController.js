import { generateToken } from "../services/common";
import userService from "../services/userService";

const handleGetAllUsers = async (req, res) => {
  const users = await userService.getAllUsers();
  return res.status(200).json({
      errCode: 0,
      errMessage: 'OK',
      users
  })
}

const handleGetUser = async (req, res) => {
  const { id } = req.params;

  if (!id) {
    return res.status(200).json({
      errCode: 1,
      errMessage: 'Missing required parameters',
      users: []
    })
  }

  const users = await userService.getUser(id);
  return res.status(200).json({
    errCode: 0,
    errMessage: 'OK',
    users
  })
}

const handleLogin = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(500).json({
      errCode: 1,
      message: 'Missing inputs parameter!'
    })
  }

  const userData = await userService.handleUserLogin(email, password);
  const { errCode } = userData;

  if (errCode === 0) {
    const token = generateToken(userData.user);
    const { accessToken, refreshToken } = token;

    userData.user.accessToken = accessToken;
    userData.user.refreshToken = refreshToken;

    await userService.updateToken(userData.user);

    // delete userData.user.id;
    delete userData.user.roleId;
  }

  return res.status(200).json({
    errCode: userData.errCode,
    message: userData.errMessage,
    user: userData.user ? userData.user : {}
  })
}

const handleRefreshToken = async (req, res) => {
  const message = await userService.refreshToken(req.body);
  return res.status(200).json(message);
}

const handleCreateNewUser = async (req, res) => {
  const message = await userService.createNewUser(req.body);
  return res.status(200).json(message);
}

const handleDeleteUser = async (req, res) => {
  if (!req.params.id) {
    return res.status(200).json({
      errCode: 1,
      errMessage: "Missing required parameters!"
    })
  }
  const message = await userService.deleteUser(req.params.id);
  return res.status(200).json(message);
}

const handleEditUser = async (req, res) => {
  const data = req.body;
  const { id } = req.params;

  const message = await userService.updateUserData(id, data);
  return res.status(200).json(message)
}

module.exports = {
  handleGetAllUsers,
  handleGetUser,
  handleRefreshToken,
  handleLogin,
  handleCreateNewUser,
  handleEditUser,
  handleDeleteUser,
}