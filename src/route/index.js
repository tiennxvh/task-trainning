import express from "express";
import userController from "../controllers/userController";
import verifyToken from "../middleware/validatorToken";
import checkPermissions from "../middleware/checkPermissions";
const router = express.Router();

const initWebRoutes = (app) => {
    router.get('/hi', (req, res, next) => { 
        res.end('hello world!');
    });

    router.post('/login', userController.handleLogin);
    router.post('/refresh-token', userController.handleRefreshToken);

    router.get('/user', checkPermissions, userController.handleGetAllUsers);
    router.get('/user/:id', checkPermissions, userController.handleGetUser);
    router.post('/user', checkPermissions, userController.handleCreateNewUser);
    router.put('/user/:id', checkPermissions, userController.handleEditUser);
    router.delete('/user/:id', checkPermissions, userController.handleDeleteUser);

    return app.use("/api", verifyToken, router);
}

module.exports = initWebRoutes;