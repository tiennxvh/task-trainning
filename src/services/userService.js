import db from "../models/index";
import bcrypt from 'bcryptjs';
import fs from 'fs';
import path from 'path';
import { generateToken } from "./common";

const jwt = require('jsonwebtoken');
const salt = bcrypt.genSaltSync(10);

const publicKey = fs.readFileSync(path.join(__dirname, '../key/public_key.pem'));

const hashUserPassword = (password) => {
  return new Promise(async (resolve, reject) => {
    try {
      const hashPassword = bcrypt.hashSync(password, salt);
      resolve(hashPassword);
    } catch (e) {
      reject(e);
    }
  })
}

const handleUserLogin = (email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userData = {};

      const isExist = await checkUserEmail(email);
      if (isExist) {
        const user = await db.User.findOne({
          attributes: ['id', 'email', 'roleId', 'password', 'username'],
          where: { email },
          raw: true
        });

        if (user) {
          const check = bcrypt.compareSync(password, user.password);
          if (check) {
            userData.errCode = 0;
            userData.errMessage = 'Ok';

            delete user.password;
            userData.user = user;
          } else {
            userData.errCode = 3;
            userData.errMessage = 'Wrong password';
          }
        } else {
          userData.errCode = 2;
          userData.errMessage = `User's not found~`
        }
      } else {
        //return error
        userData.errCode = 1;
        userData.errMessage = `Your's Email isn't exist in your system. Plz try other email!`
      }
      resolve(userData)
    } catch (e) {
      reject(e)
    }
  })
}

const checkUserEmail = (userEmail) => {
  return new Promise(async (resolve, reject) => {
    try {
      const user = await db.User.findOne({
        where: { email: userEmail }
      })
      if (user) {
        resolve(true)
      } else {
        resolve(false)
      }
    } catch (e) {
      reject(e);
    }
  })
}

const checkUsername = (username) => {
  return new Promise(async (resolve, reject) => {
    try {
      const user = await db.User.findOne({
        where: { username }
      })
      if (user) {
        resolve(true)
      } else {
        resolve(false)
      }
    } catch (e) {
      reject(e);
    }
  })
}

const createNewUser = (data) => {
  const { email, username } = data;

  return new Promise(async (resolve, reject) => {
    try {
      const check = await checkUserEmail(email);
      const isInValidUsername = await checkUsername(username);

      if (check) {
        resolve({
          errCode: 1,
          errMessage: 'Your email is already in used, Plz try another email!'
        })
      } else if (isInValidUsername) {
        resolve({
          errCode: 1,
          errMessage: 'Your username is already in used, Plz try another username!'
        })
      } else {
        const hashPasswordFromBcrypt = await hashUserPassword(data.password);

        await db.User.create({
          username: data.username,
          email: data.email,
          password: hashPasswordFromBcrypt,
          roleId: data.roleId
        })

        resolve({
          errCode: 0,
          message: 'Create information successfully!'
        })
      }
    } catch (e) {
      reject(e);
    }
  })
}

const deleteUser = (userId) => {
  return new Promise(async (resolve, reject) => {
    const foundUser = await db.User.findOne({
      where: { id: userId }
    })
    if (!foundUser) {
      resolve({
        errCode: 2,
        errMessage: `The user isn't exist`
      })
    }

    await db.User.destroy({
      where: { id: userId }
    })

    resolve({
      errCode: 0,
      message: `The user is deleted`
    })
  })
}

const updateUserData = (id, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!id || !data.password) {
        resolve({
          errCode: 2,
          errMessage: 'Missing required parameters'
        })
      }
      const user = await db.User.findOne({
        where: { id },
        raw: false
      })
      if (user) {
        const hashPasswordFromBcrypt = await hashUserPassword(data.password);
        user.password = hashPasswordFromBcrypt;

        await user.save();

        resolve({
          errCode: 0,
          message: 'Update the user succeeds!'
        })
      } else {
        resolve({
          errCode: 1,
          errMessage: `User's not found!`
        });
      }
    } catch (e) {
      reject(e);
    }
  })
}

const updateToken = (data) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (!data.id || !data.refreshToken || !data.accessToken) {
        resolve({
          errCode: 2,
          errMessage: 'Missing required parameters'
        })
      }

      const user = await db.User.findOne({
        where: { id: data.id },
        raw: false
      })

      if (user) {
        user.refreshToken = data.refreshToken;
        user.accessToken = data.accessToken;

        await user.save();

        resolve({
          errCode: 0,
          message: 'Update the token succeeds!'
        })
      } else {
        resolve({
          errCode: 1,
          errMessage: `User's not found!`
        });
      }
    } catch (e) {
      reject(e);
    }
  })
}

const refreshToken = (data) => {
  const { refreshToken } = data;

  return new Promise(async (resolve, reject) => {
    try {
      if (!refreshToken) {
        resolve({
          errCode: 2,
          errMessage: 'Missing required parameters'
        })
      }

      const user = await db.User.findOne({
        where: { refreshToken },
        raw: false
      })

      if (!user) {
        resolve({
          errCode: 0,
          message: 'Not found refresh token!'
        })
      }

      try {
        jwt.verify(refreshToken, publicKey, { algorithms: ['RS256'] });

        const token = generateToken(user);
        const { accessToken, refreshToken: nRefreshToken } = token;

        user.accessToken = accessToken;
        user.refreshToken = nRefreshToken;

        await updateToken(user);

        resolve({
          // user,
          accessToken,
          refreshToken: nRefreshToken,
          errCode: 0,
          message: 'Refresh token succeeds!'
        })
      } catch (error) {
        reject(error);
      }
    } catch (e) {
      reject(e);
    }
  })
}

const getUser = (userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const users = await db.User.findOne({
        where: { id: userId },
        attributes: {
          exclude: ['password']
        }
      })

      resolve(users)

    } catch (e) {
      reject(e);
    }
  })
}

const getAllUsers = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const users = await db.User.findAll({
        attributes: {
          exclude: ['password']
        }
      });

      resolve(users)
    } catch (e) {
      reject(e);
    }
  })
}

module.exports = {
  getAllUsers,
  getUser,
  refreshToken,
  updateToken,
  handleUserLogin,
  createNewUser,
  deleteUser,
  updateUserData,
}