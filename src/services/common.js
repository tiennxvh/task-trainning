import fs from 'fs';
import path from 'path';

const jwt = require('jsonwebtoken');
const privateKey = fs.readFileSync(path.join(__dirname, '../key/private_key.pem'));

export const generateToken = (payload) => {
  const { id, username, email } = payload;

  return {
    accessToken: jwt.sign({ id, username, email }, privateKey, { algorithm: 'RS256', expiresIn: '1h' }),
    refreshToken: jwt.sign({ id, username, email }, privateKey, { algorithm: 'RS256', expiresIn: '1h' })
  }
}